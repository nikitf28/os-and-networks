<?php
	$nouns = array();
	$adj = array();
	$first_names = array();
	$second_names = array();

	$file = fopen(__DIR__ . '/nouns.txt', 'r');
	while (!feof($file)) {
    		array_push($nouns, fgets($file));
	}
	fclose($file);

	$file = fopen(__DIR__ . '/adjectives.txt', 'r');
        while (!feof($file)) {
                array_push($adj, fgets($file));
        }
        fclose($file);



        $file = fopen(__DIR__ . '/first_names.txt', 'r');
        while (!feof($file)) {
                array_push($first_names, fgets($file));
        }
        fclose($file);

        $file = fopen(__DIR__ . '/second_names.txt', 'r');
        while (!feof($file)) {
                array_push($second_names, fgets($file));
        }
        fclose($file);

	//$db = new PDO("mysql:dbname=web-test;host=localhost;", "web_site", "super618");

	$time_start = microtime(true);

	$db = new PDO("mysql:dbname=db1;host=rc1b-0dwfiueivc1bmrk6.mdb.yandexcloud.net;", "user1", "super618",
		array(
			PDO::MYSQL_ATTR_SSL_CA    => '/home/nikita/.mysql/root.crt'
		)
	);


	for ($i = 0; $i < 10000; $i++){

		$first_part = substr_replace($adj[rand(0, count($adj))],"", -1);
		$second_part = substr_replace($nouns[rand(0, count($nouns))], "", -1);
		$film_name = $first_part." ".$second_part;

        	$first_part = substr_replace($first_names[rand(0, count($first_names) - 1)],"", -1);
        	$second_part = substr_replace($second_names[rand(0, count($second_names) - 1)], "", -1);
        	$director_name = $first_part." ".$second_part;


		$rating = rand(1000, 9999) / 1000.0;
		$year = rand(1980, 2022);
		$min_age_array = array(0, 6, 12, 16, 18);
		$min_age = $min_age_array[rand(0, count($min_age_array) - 1)];

		$sql = "INSERT INTO `Films` (`title`, `director`, `rating`, `year`, `min_age`) VALUES(\"".$film_name."\", \"".$director_name."\", \"".$rating."\", \"".$year."\", \"".$min_age."\");";
		echo $sql."\n";
		$db->prepare($sql)->execute();

		echo $i." ".$film_name." ".$director_name." ".$rating." ".$year." ".$min_age."\n";
	}

	$time_end = microtime(true);
	$time = $time_end - $time_start;

	echo "\n";
	echo "\n";
	echo "\n";
	echo "Script time: ".$time;
?>
