import base64
from random import randint
import math

BITS = 7


def getDegrees(num):
    degress = []
    i = 0
    while num > 0:
        digit = num % 2
        if digit == 1:
            degress.append(2 ** i)
        i += 1
        num//=2
    return degress


def NOD(a, b):
    while a != b:
        print(a, b)
        if a > b:
            a = a % b
        else:
            b = b % a
    return max(a, b)


def isPrime(a):
    for i in range(2, int(math.sqrt(a))):
        if a % i == 0:
            return False
    return True


def isCoprime(a, b):
    for i in range(2, min(a, b) + 1):
        if a % i == 0 and b % i == 0:
            return False
    return True


def gen_keys():
    p = randint(2 << BITS, (2 << (BITS + 1)) - 1)
    while not isPrime(p):
        p = randint(2 << BITS, (2 << (BITS + 1)) - 1)
        # print(p)

    q = randint(2 << BITS, (2 << (BITS + 1)) - 1)
    while not isPrime(q):
        q = randint(2 << BITS, (2 << (BITS + 1)) - 1)
        # print(p)

    n = q * p
    fe = (p - 1) * (q - 1)

    e = 0

    for i in range(fe // 100, fe - 1):
        if isCoprime(i, fe):
            e = i
            break

    #print(p, q, fe, e)

    d = 0

    for i in range(2, 2 << 100):
        # if i % 1000 == 0:
        # print(e, i, fe, e * i, (e * i) % fe)
        if (e * i) % fe == 1:
            # print(e, i, fe, e * i, (e * i) % fe)
            d = i
            break

    print("Public key: %s, %s" % (e, n))
    print("Private key: %s, %s" % (d, n))
    return e, d, n

def enCode(e, n, msg):
    encrypted_info = []
    degrees = getDegrees(e)
    for l in msg:
        crypted_word = 1
        for degree in degrees:
            for i in range(degree):
                crypted_word *= l
                crypted_word %= n
        encrypted_info.append(crypted_word)
    return encrypted_info


def deCode(d, n, msg):
    encrypted_info = []
    degrees = getDegrees(d)
    for l in msg:
        decrypted_word = 1
        for degree in degrees:
            for i in range(degree):
                decrypted_word *= l
                decrypted_word %= n
        encrypted_info.append(decrypted_word)
    return encrypted_info