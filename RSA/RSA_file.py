import RSA_UTIL

n, e, d = map(int, input('Input n, e, d: ').split())

file_name = input('Input the file name: ')

decision = input("0 - encode, 1 - decode: ")

f = open(file_name, 'rb')

if decision == '0':
    data = f.read()
    f.close()
    crypto = RSA_UTIL.enCode(e, n, data)
    cryptoBytes = []
    for cr in crypto:
        bytes_val = cr.to_bytes(3, 'big')
        cryptoBytes += bytes_val
    f = open(file_name + '.crypt', 'wb')
    #print(cryptoBytes)
    f.write(bytes(cryptoBytes))
    f.close()

if decision == '1':
    data = f.read()
    f.close()
    normData = []
    i = 0
    while i < len(data):
        num = int.from_bytes(bytes(data[i:i+3]), 'big')
        normData.append(num)
        i+= 3
    decrypto = RSA_UTIL.deCode(d, n, normData)
    #print(normData)
    #print(decrypto)
    f = open(file_name + '.decrypt', 'wb')
    f.write(bytes(decrypto))
    f.close()

# 190637 1901 10481
# 18.jpg.crypt