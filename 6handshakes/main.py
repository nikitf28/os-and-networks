def dfs(person_id, root_person, distances, visited, friend_list, depth):
    print(visited[person_id])
    if visited[person_id]:
        return
    visited[person_id] = True
    distances[root_person][person_id] = depth
    if person_id not in friend_list:
        return
    for person in friend_list[person_id]:
        dfs(person, root_person, distances, visited, friend_list, depth+1)

f = open('facebook_combined.txt')

friend_list = {}


for line in f:
    person_a, person_b = map(int, line.split())
    if person_a not in friend_list:
        friend_list[person_a] = []
    friend_list[person_a].append(person_b)
    if person_b not in friend_list:
        friend_list[person_b] = []
    friend_list[person_b].append(person_a)

f.close()

people_amount = max(friend_list.keys()) + 1

distances = [[9999 for i in range(people_amount)] for i in range(people_amount)]
visited = [False for i in range(people_amount)]

for i in range(0, people_amount):
    print(i, people_amount)
    dfs(i, i, distances, list(visited), friend_list, 0)

f = open('output.txt', 'w')

f.write(str(distances))

f.close()