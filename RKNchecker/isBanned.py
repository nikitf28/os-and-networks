import re
import socket

import blackList

RKNblackList = blackList.getBlacklist()


while True:
    print('Enter URL to check it in RKN blacklist: ')
    URL = input()

    print('\n\n1. Checking banning by URL...')
    URLban = RKNblackList.isURLBanned(URL)
    if URLban is None:
        print('URL is no banned..')
    else:
        print(URLban)
    print('______________________________\n')


    domain = re.search(r'https?://([A-Za-z_0-9.-]+).*', URL).group(1)
    print('2. Checking by domain: ', domain)
    doaminBan = RKNblackList.isDomainBanned(domain)
    if doaminBan is None:
        print('Domain is not banned!')
    else:
        print(doaminBan)
    print('______________________________\n')

    ip = socket.gethostbyname(domain)
    print('3. Checking by IP:', ip)
    ipBan = RKNblackList.isIPBanned(ip)
    if ipBan is None:
        print('IP is not banned')
    else:
        print(ipBan)
    print('______________________________\n\n\n\n\n')