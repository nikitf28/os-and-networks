import json

import update


class BlackList:
    IPs = []
    date = ''
    domain = ''
    URL = ''
    oder = ''
    issuedBy = ''

    def __init__(self, IPs, date, domain, URL, order, issuedBy):
        self.IPs = IPs
        self.date = date
        self.domain = domain
        self.URL = URL
        self.order = order
        self.issuedBy = issuedBy

    def __str__(self):
        output = "URL %s was banned %s.\nDomain: %s.\nIPs: %s.\n" \
                 "By %s. Order: %s." % (self.URL, self.date, self.domain, self.IPs, self.issuedBy, self.order)
        return output

    def isIPBanned(self, IP):
        if IP in self.IPs:
            return True
        return False

    def isURLBanned(self, URL):
        if URL[-1] == '/':
            URL = URL[:-1]
        if self.URL == URL:
            return True
        return False

    def isDomainBanned(self, domain):
        if self.domain == domain:
            return True
        return False


class BlackListArray:
    blackList = []

    def isIPBanned(self, IP):
        for position in self.blackList:
            if position.isIPBanned(IP):
                return position
        return None

    def isURLBanned(self, URL):
        for position in self.blackList:
            if position.isURLBanned(URL):
                return position
        return None

    def isDomainBanned(self, domain):
        for position in self.blackList:
            if position.isDomainBanned(domain):
                return position
        return None

    def __init__(self, filename):
        with open(filename, 'r') as read_file:
            data = json.load(read_file)
        dateKey = list(data.keys())[0]
        jsonData = data[dateKey]
        for jsonPostion in jsonData:
            IPs = jsonPostion['ip']
            date = jsonPostion['date']
            domain = jsonPostion['page']
            URL = jsonPostion['link']
            order = jsonPostion['postanovlenie']
            issuedBy = jsonPostion['gos_organ']
            blackListPostion = BlackList(IPs, date, domain, URL, order, issuedBy)
            self.blackList.append(blackListPostion)


def getBlacklist():
    filename = update.update()
    print('Blacklist filename is %s' % filename)
    return BlackListArray(filename)
