**Исследование распределения сетевой задержки**

Пропинговали сайт twitter.com. Среднее время отклика колеблится в пределах 110мс. Осуществлили 2000 пинг-запросов, использовали ЯП Python. Полный список времени отклика можно посмотреть в [output.txt](https://gitlab.com/nikitf28/os-and-networks/-/blob/master/lattency/output.txt).

График распределения:

<img src="https://gitlab.com/nikitf28/os-and-networks/-/raw/master/lattency/Figure_1.png"/>
