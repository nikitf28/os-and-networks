def getMask(ip):
    mask = ''
    strBytes = ['', '', '', '']
    byte = 3
    slash = False
    for i in range(len(ip) - 1, -1, -1):
        if ip[i] == '/':
            slash = True
            continue
        if slash:
            if ip[i] == '.':
                byte -= 1
                continue
            strBytes[byte] = ip[i] + strBytes[byte]
        else:
            mask = ip[i] + mask
    ip = int(strBytes[0]) * (2**24) + int(strBytes[1]) * (2**16) + int(strBytes[2]) * (2**8) + int(strBytes[3])
    return int(mask), ip


def countByMask(ips):
    ipsNum = 0
    for ip in ips:
        mask = getMask(ip)[0]
        ipsNum += 2 ** (32 - mask)
    return ipsNum

def countByIP(ips):
    ipsList = set()
    for ip in ips:
        mask, ipInt = getMask(ip)
        startIP = (ipInt >> (32 - mask)) << (32 - mask)
        endIP = startIP + (2 ** (32 - mask) - 1)
        for i in range(startIP, endIP + 1):
            ipsList.add(i)
    return len(ipsList)

ips = []
f = open('blocked-networks.txt')
for line in f:
    ips.append(line)
f.close()

way1 = countByMask(ips)
way2 = countByIP(ips)

print(way1)
print(way2)