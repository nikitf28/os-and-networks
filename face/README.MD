**Программа вырезания фрагментов с лицами из изображения**

Написал программу на python (файл `main.py`), берёт из текущего каталога файл `input.jpg`, отправляет его на сервера Microsoft (с использованием собственного ключа), получает координаты лиц, режет их на файлы и кладёт в каталог `output`.

Входное фото:

<img alt="original photo" src="./input.jpg">

Нарезанные лица:

<img alt="face1" src="./output/output1.jpg">
<img alt="face2" src="./output/output2.jpg">
<img alt="face3" src="./output/output3.jpg">
<img alt="face4" src="./output/output4.jpg">
<img alt="face5" src="./output/output5.jpg">
<img alt="face6" src="./output/output6.jpg">
