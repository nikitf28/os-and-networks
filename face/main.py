import requests
from PIL import Image
import os

for file in os.listdir('output'):
    os.remove('output/' + file)

url = 'https://bki-face.cognitiveservices.azure.com/face/v1.0/detect?returnFaceId=true'

with open('./input.jpg', 'rb') as f:
    data = f.read()

headers = {
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': 'f09ade68966c4efeafa6433431b0feae'
}


resp = requests.post(url, headers=headers, data=data)

faces = resp.json()

im = Image.open('input.jpg')

i = 1

for face in faces:
    top = int(face['faceRectangle']['top'])
    left = int(face['faceRectangle']['left'])
    width = int(face['faceRectangle']['width'])
    height = int(face['faceRectangle']['height'])
    im_crop = im.crop((left, top, left+width, top+height))
    im_crop.save('output/output' + str(i) + '.jpg')
    i += 1