import cv2

video_fn = 'input/video.mp4'

def key_crop():
    key_data = []
    f = open('coords.txt', 'r')
    for line in f:
        data = list(map(int, line.split(' ')))
        key_data.append({'frame': int(data[0]), 'top': int(data[1]), 'left': int(data[2]),
               'width': int(data[3]), 'height': int(data[4])})
    key_data = sorted(key_data, key=lambda d: d['frame'])
    cap = cv2.VideoCapture(video_fn)
    out = cv2.VideoWriter('crops/cool.avi', cv2.VideoWriter_fourcc('F', 'M', 'P', '4'), 25,
                          (300, 700))
    for i in range(len(key_data) - 1):
        key = key_data[i]
        print(key['frame'])
        next_key = key_data[i + 1]
        width = key['width']
        height = key['height']
        left = key['left']
        top = key['top']

        xB = max(left - width*2, 0)
        xE = min(left + width*2, 1280)
        yB = max(top - height*2, 0)
        yE = min(top + height*12, 720)
        for frame_id in range(key['frame'], next_key['frame']):
            cap.set(cv2.CAP_PROP_POS_FRAMES, frame_id)
            ret, frame = cap.read()
            resized = cv2.resize(frame[yB:yE, xB:xE], (300, 700), interpolation=cv2.INTER_AREA)
            out.write(resized)
    out.release()
