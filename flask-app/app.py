from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
   "https://c.tenor.com/YjeDKHDpa6gAAAAd/cool-cat.gif",
   "https://c.tenor.com/gIaioChTOloAAAAC/cat-cute.gif",
   "https://c.tenor.com/GTcT7HODLRgAAAAC/smiling-cat-creepy-cat.gif",
   "https://c.tenor.com/ZhfMGWrmCTcAAAAC/cute-kitty-best-kitty.gif",
   "https://c.tenor.com/Ao9O4SGI-cQAAAAd/sleep-cat-two-cat.gif",
   "https://c.tenor.com/SN6EKkspK5UAAAAd/suspense-suspicious.gif",
   "https://c.tenor.com/uzWDSSLMCmkAAAAd/cute-cat-oh-yeah.gif",
   "https://c.tenor.com/2T506UHvonMAAAAC/pirate-cat.gif",
   "https://c.tenor.com/I_DYaT5W9JkAAAAd/cat-stacked.gif"]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
