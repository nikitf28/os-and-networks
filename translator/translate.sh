API_KEY='460284435f694b9e94beca884c6863b3'
REGION='germanywestcentral'

ifile=""
ofile="translated.txt"
lang=""

while [ -n "$1" ]
do
case "$1" in
-i) ifile=$2;;
-o) ofile=$2;;
-l) lang=$2;;
--) shift
break ;;
esac
shift
done

if [ -z "$lang" ]
then
echo "Language is not specified"
exit
fi

if [ -z "$ifile" ]
then
echo "Input file is not specified"
exit
fi

text="`cat $ifile`"

translated="`curl -X POST "https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to=$lang" \
     -H "Ocp-Apim-Subscription-Key:$API_KEY" \
     -H "Ocp-Apim-Subscription-Region:$REGION" \
     -H "Content-Type: application/json" \
     -d "[{'Text':'$text'}]" | jq  '.[0].translations[0].text'`"


echo -e $translated > $ofile

