let title, mainText, yes_btn, no_btn, image, mainWindow;
let goto_yes = 'q1';
let goto_no = 'q1';
let goto_now = '';

function load_content(){
    let now = questons.get(goto_now);
    title.textContent = '';
    mainText.textContent = '';
    image.src = '';
    if (now.type === 'q'){
        yes_btn.textContent = yes_text;
        no_btn.textContent = no_text;
        goto_yes = now.positive;
        goto_no = now.negative;
        title.textContent = now.text;
        mainWindow.style.height = "400px";
        if (now.image){
            image.style.display = "block";
            image.src = 'images/questions/' + now.image;
        }
        else{
            image.style.display = "none";
        }

    }
    else if (now.type === 'a'){
        goto_yes = 'share';
        goto_no = 'donate';
        title.textContent = now.text;
        image.src = 'images/db_logos/' + now.logo;
        image.style.display = "block";
        mainText.innerHTML = get_final_text(goto_now);
        mainWindow.style.height = "500px";
        yes_btn.textContent = share;
        no_btn.textContent = donate;
    }
}

function yes_handler(){
    if (goto_yes === 'share'){
        window.open(generate_share_link(goto_now), "_blank");
        return;
    }
    goto_now = goto_yes;
    load_content();
}

function no_handler(){
    if (goto_no === 'donate'){
        window.open(donate_link, "_blank");
        return;
    }
    goto_now = goto_no;
    load_content();
}

function init(){
    title = document.getElementById('title');
    mainText = document.getElementById('mainText');
    yes_btn = document.getElementById('yes_btn');
    no_btn = document.getElementById('no_btn');
    image = document.getElementById('image');
    mainWindow = document.getElementById('mainWindow');

    title.textContent = init_title;
    mainText.textContent = init_text;
    yes_btn.textContent = init_yes;
    no_btn.textContent = init_no;

    yes_btn.addEventListener("click", yes_handler);
    no_btn.addEventListener("click", no_handler);
}

document.addEventListener("DOMContentLoaded", init);