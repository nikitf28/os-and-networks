const page_link = "https://nikitf28.gitlab.io/os-and-networks/DBchoice";

const questons = new Map([
    ['q1', {text: 'Данные структурированы?', type: 'q', positive: 'q2', negative: 'q3', image: false}],
    ['q2', {text: 'Данных много?', type: 'q', positive: 'q4', negative: 'q3', image: false}],
    ['q3', {text: 'Используется стек MEAN?', type: 'q', positive: 'q8', negative: 'q14', image: 'q3.jpg'}],
    ['q4', {text: 'Нагрузка смешанная?', type: 'q', positive: 'a8', negative: 'q5', image: false}],
    ['q5', {text: 'Нужен технологически независимый стек?', type: 'q', positive: 'q6', negative: 'q10', image: false}],
    ['q6', {text: 'Нужна быстрая разработка?', type: 'q', positive: 'q13', negative: 'q7', image: false}],
    ['q7', {text: 'Есть сложные операции с данными?', type: 'q', positive: 'q11', negative: 'a4', image: false}],
    ['q8', {text: 'Основная цель - аналитически запросы?', type: 'q', positive: 'q12', negative: 'q15', image: false}],
    ['q9', {text: 'Используются бессерверные вычисления?', type: 'q', positive: 'a1', negative: 'q6', image: 'q9.jpeg'}],
    ['q10', {text: 'Технологии Microsoft?', type: 'q', positive: 'a2', negative: 'a1', image: 'q10.jpg'}],
    ['q11', {text: 'Есть администратор баз данных?', type: 'q', positive: 'a3', negative: 'a4', image: 'q11.jpg'}],
    ['q12', {text: 'Нужен ли полнотекстовый поиск?', type: 'q', positive: 'a5', negative: 'q14', image: false}],
    ['q13', {text: 'Используется PHP?', type: 'q', positive: 'a4', negative: 'a3', image: 'q13.jpg'}],
    ['q14', {text: 'Нужно изменять данные?', type: 'q', positive: 'a5', negative: 'a6', image: false}],
    ['q15', {text: 'Данных много?', type: 'q', positive: 'a8', negative: 'a7', image: false}],
    ['a1', {text: 'Yandex Database', type: 'a', logo: 'Yandex_DB.png', link: 'https://cloud.yandex.ru/services/ydb'}],
    ['a2', {text: 'Microsoft SQL', type: 'a', logo: 'MS_SQL.jpg', link: 'https://www.microsoft.com/ru-ru/sql-server/sql-server-2019'}],
    ['a3', {text: 'PostgreSQL', type: 'a', logo: 'PostgreSQL.jpg', link: 'https://www.postgresql.org/'}],
    ['a4', {text: 'MySQL', type: 'a', logo: 'my_sql.png', link: 'https://www.mysql.com/'}],
    ['a5', {text: 'Elasticsearch', type: 'a', logo: 'elasticsearch.png', link: 'https://www.elastic.co/elasticsearch/'}],
    ['a6', {text: 'ClickHouse', type: 'a', logo: 'click_house.png', link: 'https://clickhouse.com/'}],
    ['a7', {text: 'Redis', type: 'a', logo: 'redis.png', link: 'https://redis.io/'}],
    ['a8', {text: 'MongoDB', type: 'a', logo: 'mongo_db.jpg', link: 'https://www.mongodb.com/'}]]
);

const init_title = "Мастер выбора СУБД";
const init_text = "Решил начать новый проект, но не знаешь, какая система управления" +
    " базами данных лучше тебе подходит? Ответь на несколько вопросов и наш супер-умный" +
    " ИИ подберёт СУБД для твоих потребностей.";
const init_yes = "Начать";
const init_no = "Тоже начать";

const yes_text = "Да";
const no_text = "Нет";

const donate_link = 'https://www.tinkoff.ru/cf/79IMHt2fb6k';

const share = "Поделиться в ВК";
const donate = "Разрабу на кофе";

function get_final_text(db_index){
    let entity = questons.get(db_index);
    let text = 'Тебе больше всего подходит ' + entity.text + '. ' +
        'Подробнее про эту СУБД ты можешь почитать <a href=\"' + entity.link +
        '\">здесь</a>.';
    return text;
}

function generate_share_link(db_index){
    let entity = questons.get(db_index);
    let share_link = "http://vk.com/share.php?url=" + page_link + "&title=" +
        "Мне рекомендовали " + entity.text + ". А какую СУБД подберут тебе?&image=" + page_link + "/images/db_logos/"
        + entity.logo +  "&noparse=true";
    return share_link;
}